FROM alpine:latest
MAINTAINER sum01 <sum01@protonmail.com>
RUN apk update \
	&& apk add \
		cmake \
		db \
		db-dev \
		g++ \
		make \
	&& rm -rf /etc/apk/cache/*
